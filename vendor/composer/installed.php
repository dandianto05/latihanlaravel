<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => '16abb5c182978e86e60b6a7d71998cceffc65429',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => '16abb5c182978e86e60b6a7d71998cceffc65429',
    ),
    'bacon/bacon-qr-code' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f73543ac4e1def05f1a70bcd1525c8a157a1ad09',
    ),
    'dasprid/enum' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5abf82f213618696dda8e3bf6f64dd042d8542b2',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v8.48.1',
      'version' => '8.48.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe4a74465a43829d2067b7e7f9c49078506ff289',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v8.48.1',
      'version' => '8.48.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '199fcedc161ba4a0b83feaddc4629f395dbf1641',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v8.48.1',
      'version' => '8.48.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '300aa13c086f25116b5f3cde3ca54ff5c822fb05',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v8.48.1',
      'version' => '8.48.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d925ec70e060a5a16949c79216389c66746a07f',
    ),
    'jaybizzle/crawler-detect' => 
    array (
      'pretty_version' => 'v1.2.106',
      'version' => '1.2.106.0',
      'aliases' => 
      array (
      ),
      'reference' => '78bf6792cbf9c569dc0bf2465481978fd2ed0de9',
    ),
    'jenssegers/agent' => 
    array (
      'pretty_version' => 'v2.6.4',
      'version' => '2.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'daa11c43729510b3700bc34d414664966b03bffe',
    ),
    'laravel/fortify' => 
    array (
      'pretty_version' => 'v1.7.13',
      'version' => '1.7.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '1753d05c80f930cc2715051858900965b49f32d9',
    ),
    'laravel/jetstream' => 
    array (
      'pretty_version' => 'v2.3.10',
      'version' => '2.3.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfe20b1462b9f8994861496ad72a3a3c7f30f553',
    ),
    'league/commonmark' => 
    array (
      'pretty_version' => '1.6.4',
      'version' => '1.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3c8b7217c52572fb42aaf84211abccf75a151b2',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.37',
      'version' => '2.8.37.0',
      'aliases' => 
      array (
      ),
      'reference' => '9841e3c46f5bd0739b53aed8ac677fa712943df7',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.49.0',
      'version' => '2.49.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '93d9db91c0235c486875d22f1e08b50bdf3e6eee',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f34c2b11eb9d2c9318e13540a1dbc2a3afbd939c',
    ),
    'pragmarx/google2fa' => 
    array (
      'pretty_version' => '8.0.0',
      'version' => '8.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '26c4c5cf30a2844ba121760fd7301f8ad240100b',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df51500adbaebdc4c38dea4c89a2e131c45c8a1',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.3.2',
      'version' => '5.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e2603bcc598e14804c4d2359d8dc4ee3c40391b',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
  ),
);
