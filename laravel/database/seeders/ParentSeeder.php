<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ParentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i<=8; $i++){
            DB::table('parents')->insert([
                [
                    'nik' => Str::random(10),
                    'nama' => Str::random(10),
                    'tanggal_lahir' => Str::random(10),
                    'alamat' => Str::random(10),
                    'status' => Str::random(10),
                ],
            ]);
        }
    }
}
