<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Num;
use Faker\Factory as Faker;


class ChildSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for($i = 1; $i<=10; $i++){
            DB::table('children')->insert([
                [
                    'parent_id' => $faker->numberBetween(25,40),
                    'nik' => Str::random(10),
                    'nama' => Str::random(10),
                    'tanggal_lahir' => Str::random(10),
                    'anak_ke' => Str::random(10),
                ],
            ]);
        }
    }
}
