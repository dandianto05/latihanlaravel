<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');
Route::get('/profile', 'ProfileController@index')->name('profile.data');
Route::get('/parent', 'ParentController@index')->name('parent.data');
Route::get('/child', 'ChildController@index')->name('child.data');
//add data
Route::get('/profile/add', 'ProfileController@add');
Route::post('/profile/simpan', 'ProfileController@simpan');
//update data
Route::get('/profile/edit/{id}', 'ProfileController@edit');
Route::post('/profile/editdata', 'ProfileController@editdata');
//delete data
Route::get('/profile/delete/{id}', 'ProfileController@datadelete');
Route::post('/profile/deleted', 'ProfileController@deleted');


Route::get('/car', 'CarController@index');
Route::get('/car/add', 'CarController@add');



Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
