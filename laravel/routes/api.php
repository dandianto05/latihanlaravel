<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//child
Route::get('/child_data', 'ChildApiController@getAllChildDataRelasiParent');
Route::get('/child_data/{id}', 'ChildApiController@getData');
Route::post('/child_data/add', 'ChildApiController@saveData');
Route::put('/child_data/edit', 'ChildApiController@updateData');
Route::delete('/child_data/delete/{id}', 'ChildApiController@deleteData');
//parent
Route::get('/parent_data', 'ParentApiController@getAllDataParent');
Route::get('/parent_data/{id}', 'ParentApiController@getData');
Route::post('/parent_data/add', 'ParentApiController@saveData');
Route::put('/parent_data/edit', 'ParentApiController@updateData');
Route::delete('/parent_data/delete/{id}', 'ParentApiController@deleteData');


Route::get('/carNative', 'CarController@getCarsNative');
Route::get('/carData', 'CarController@getCars');