@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('table_name','Data Child')
@section('table')
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Parent ID</th>
        <th scope="col">NIK</th>
        <th scope="col">Nama</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">Anak ke</th>
        <th scope="col">Orang Tua</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data_child as $item)
      <tr>
        <th scope="row">{{ $loop ->iteration }}</th>
        <td>{{$item->parent_id}}</td>
        <td>{{$item->nik}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->tanggal_lahir}}</td>
        <td>{{$item->anak_ke}}</td>
        <td>{{$item->parents_relation->nama}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>


@endsection