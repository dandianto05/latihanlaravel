@foreach ($data as $item)
<form action="/profile/deleted" method="post">
    {{csrf_field()}}
    <input type="hidden" name="id" value="{{ $item->id }}">
    <table>
        <tr>
            <td>Nama Lengkap</td>
            <td> : {{ $item->nama_lengkap }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td> : {{ $item->email }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td> : {{ $item->alamat }}</td>
        </tr>
        <tr>
            <th colspan="2" bgcolor="lightgray"><button type="submit">hapus</button></th>
        </tr>
    </table>
@endforeach