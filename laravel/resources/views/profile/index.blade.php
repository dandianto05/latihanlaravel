@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('table_name','Data Profile')
@section('table')

<button onclick="add()">Add</button>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Nama Lengkap</th>
        <th scope="col">Email</th>
        <th scope="col">Alamat</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
      <tr>
        <th scope="row">{{ $loop ->iteration }}</th>
        <td>{{ Str::limit($item->nama_lengkap, 20, ' (...)') }}</td>
        <td>{{ Str::limit($item->email, 20, ' (...)') }}</td>
        <td>{{ Str::limit($item->alamat, 20 , ' (...)') }}</td>
        <td>
          <button value= "{{ $item->id}}" onclick="edit(this.value)">Edit</button>
          <button value= "{{ $item->id}}" onclick="delete_(this.value)">Hapus</button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <script>
    function add(){
      $.ajax({
        url:'/profile/add',
        type:'get',
        contentType:'html',
        success:function(result){
          $('#mymodal').modal('show')
          $('#judulModal').html('Form Tambah profile')
          $('.modal-body').html(result)
        }
      })
      //window.open('/profile/add/', '_self')
    }

    function edit(id){
      $.ajax({
        url:'/profile/edit/'+id,
        type:'get',
        contentType:'html',
        success:function(result){
          $('#mymodal').modal('show')
          $('#judulModal').html('Form Update profile')
          $('.modal-body').html(result)
        }
      })
      //window.open('/profile/edit/' +id, '_self')
    }
    function delete_(id){
      $.ajax({
        url:'/profile/delete/'+id,
        type:'get',
        contentType:'html',
        success:function(result){
          $('#mymodal').modal('show')
          $('#judulModal').html('Form Hapus profile')
          $('.modal-body').html(result)
        }
      })
      //window.open('/profile/delete/' +id, '_self')
    }
  </script>


@endsection