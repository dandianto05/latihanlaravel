<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="./assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="./assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="./assets/css/argon.css?v=1.1.0" type="text/css">
    <script src="/jquery-3.6.0.min.js"></script>
    <title>@yield('title')</title>
</head>

<body class="card">
    {{-- Navbar --}}
    <nav id="navbar-main" class="navbar navbar-horizontal navbar-main navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
            <h1 class="text-white" style="">Laravel Dandi Anto &nbsp&nbsp&nbsp&nbsp</h1>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse"
                aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="./pages/dashboards/dashboard.html">
                                <img src="./assets/img/brand/blue.png">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a href="{{ route('profile.data') }}" class="nav-link">
                            <span class="nav-link-inner--text">Data Profile</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('parent.data') }}" class="nav-link">
                            <span class="nav-link-inner--text">Data Parent</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('child.data') }}" class="nav-link">
                            <span class="nav-link-inner--text">Data Child</span>
                        </a>
                    </li>
                </ul>
                <hr class="d-lg-none" />
                <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="https://www.facebook.com/creativetim" target="_blank"
                            data-toggle="tooltip" title="" data-original-title="Like us on Facebook">
                            <i class="fab fa-facebook-square"></i>
                            <span class="nav-link-inner--text d-lg-none">Facebook</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="https://www.instagram.com/creativetimofficial"
                            target="_blank" data-toggle="tooltip" title="" data-original-title="Follow us on Instagram">
                            <i class="fab fa-instagram"></i>
                            <span class="nav-link-inner--text d-lg-none">Instagram</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="https://twitter.com/creativetim" target="_blank"
                            data-toggle="tooltip" title="" data-original-title="Follow us on Twitter">
                            <i class="fab fa-twitter-square"></i>
                            <span class="nav-link-inner--text d-lg-none">Twitter</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="https://github.com/creativetimofficial" target="_blank"
                            data-toggle="tooltip" title="" data-original-title="Star us on Github">
                            <i class="fab fa-github"></i>
                            <span class="nav-link-inner--text d-lg-none">Github</span>
                        </a>
                    </li>
                    <li class="nav-item d-none d-lg-block ml-lg-4">
                        <a href="https://www.creative-tim.com/product/argon-dashboard-pro" target="_blank"
                            class="btn btn-neutral btn-icon">
                            <span class="btn-inner--icon">
                                <i class="fa fa-search mr-2"></i>
                            </span>
                            <span class="nav-link-inner--text">Search now</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    {{-- modal --}}
    <div id="mymodal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="judulModal">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    {{-- Main --}}
    <div class="main-content">

        <!-- Header -->
        <div class="header bg-gradient-default pt-5 pb-7">
            <div class="container" style="margin-bottom: 10%">
                <div class="header-body">
                    <div class="row-lg-1">
                        <div class="pr-1">
                            <h3 class="display-2 text-white font-weight-bold mb-3">@yield('table_name')</h3>
                        </div>
                    </div>
                    <div class="row-lg-9">
                        <div class="row pt-10">
                            {{-- Table --}}
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="h5">@yield('table')</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section section-light pt-10 mt--7">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card card-lift--hover shadow border-0">
                                    <div class="card-body py-5">
                                        <div class="icon icon-shape bg-gradient-primary text-white rounded-circle mb-4">
                                            <i class="ni ni-planet"></i>
                                        </div>
                                        <h4 class="h3 text-primary text-uppercase">Albert Einstein</h4>
                                        <p class="description mt-3">"Berusahalah untuk tidak menjadi manusia yang
                                            berhasil
                                            tapi berusahalah menjadi manusia yang berguna."</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-lift--hover shadow border-0">
                                    <div class="card-body py-5">
                                        <div class="icon icon-shape bg-gradient-success text-white rounded-circle mb-4">
                                            <i class="ni ni-books"></i>
                                        </div>
                                        <h4 class="h3 text-success text-uppercase">Mohammad Hatta</h4>
                                        <p class="description mt-3">"Aku rela di penjara asalkan bersama buku,
                                            karena
                                            dengan
                                            buku aku bebas"</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-lift--hover shadow border-0">
                                    <div class="card-body py-5">
                                        <div class="icon icon-shape bg-gradient-warning text-white rounded-circle mb-4">
                                            <i class="ni ni-spaceship"></i>
                                        </div>
                                        <h4 class="h3 text-warning text-uppercase">M. Aan Mansur</h4>
                                        <p class="description mt-3">“ Kata 'pulang' selalu terdengar jauh lebih
                                            indah
                                            dari
                                            pada 'pergi'. Tetapi orang harus rindu untuk bisa menikmati keindahan
                                            pulang.”
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>


        <!-- Footer -->
        <footer class="py-5" id="footer-main">
            <div class="container">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="text-center text-xl-left text-muted" align="center">
                            &copy; 2021 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1"
                                target="_blank">About me</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Argon Scripts -->
        <!-- Core -->

        <script src="./assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="./assets/vendor/js-cookie/js.cookie.js"></script>
        <script src="./assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="./assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
        <!-- Optional JS -->
        <script src="./assets/vendor/onscreen/dist/on-screen.umd.min.js"></script>
        <!-- Argon JS -->
        <script src="./assets/js/argon.js?v=1.1.0"></script>


</body>

</html>
