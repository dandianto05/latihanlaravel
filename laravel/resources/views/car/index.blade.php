@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('table_name','Data Profile')
@section('table')

<button onclick="javascript:window.open('/car/add' , '_self')">Add</button>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No.</th>
        <th scope="col">ID</th>
        <th scope="col">Model</th>
        <th scope="col">Brand</th>
        <th scope="col">CC</th>
        <th scope="col">Valve</th>
        <th scope="col">Year</th>
        <th scope="col">Owner</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
      <tr>
        <th scope="row">{{ $loop ->iteration }}</th>
        <td>{{ $item->id }}</td>
        <td>{{ $item->model }}</td>
        <td>{{ $item->brand }}</td>
        <td>{{ $item->cc }}</td>
        <td>{{ $item->valve }}</td>
        <td>{{ $item->year }}</td>
        <td>{{ $item->profile_relation->nama_lengkap }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>


@endsection