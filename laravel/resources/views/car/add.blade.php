<form action="/car/simpan" method="post">
    {{csrf_field()}}
    <table>
        <tr>
            <td>Owner</td>
            <td>
                <select name="profile_id">
                    @foreach ($profile as $item)
                        <option value="{{ $item->id }}">{{$item->nama_lengkap}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        <tr>
            <td>Model</td>
            <td><input type="text" name="model" class="form-control"></td>
        </tr>
        <tr>
            <td>Brand</td>
            <td><input type="text" name="brand" class="form-control"></td>
        </tr>
        <tr>
            <td>CC</td>
            <td><input type="text" name="cc" class="form-control"></td>
        </tr>
        <tr>
            <td>Valve</td>
            <td><input type="text" name="valve" class="form-control"></td>
        </tr>
        <tr>
            <td>Year</td>
            <td><input type="text" name="year" class="form-control"></td>
        </tr>
        <tr>
            <td ><input class="btn-primary" type="submit" value="submit"> </td>
        </tr>
    </table>