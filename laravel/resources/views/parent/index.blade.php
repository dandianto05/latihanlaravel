@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('table_name','Data Parent')
@section('table')

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No.</th>
        <th scope="col">NIK</th>
        <th scope="col">Nama</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">Alamat</th>
        <th scope="col">Status</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data_parents as $item)
      <tr>
        <th scope="row">{{ $loop ->iteration }}</th>
        <td>{{$item->nik}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->tanggal_lahir}}</td>
        <td>{{$item->alamat}}</td>
        <td>{{$item->status}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>




@endsection