@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('content')
    <table class="table table-borderless table-dark" align = "center"  border= "0" cellpadding = "5" cellspacing = "0" width = "50%">
        <tbody>
            <tr>
                <th scope="row">Nama</th>
                <td>Dandi Anto</td>
            </tr>
            <tr>
                <td>Asal Jurusan</td>
                <td>Teknik Infromatika</td>
            </tr>
            <tr>
                <td>Jenjang Akademik</td>
                <td>
                    <select>
                        <option value="S1">Strata 1</option>
                        <option value="S2">Strata 2</option>
                        <option value="S3">Strata 3</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td><textarea rows = "7" cols="50">Pleret</textarea></td>
            </tr>
            <tr>
                <td>Hobi</td>
                <td>
                    <input type="checkbox" name="cek" id="cek1">Game<br>
                    <input type="checkbox" name="cek" id="cek2">Coffe<br>
                </td>
            </tr>
        </tbody>
    </table>
@endsection
