<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    use HasFactory;
    protected $table = 'children';

    public function parents_relation(){
        return $this->belongsTo('App\Models\Parents','parent_id');
    }
}
