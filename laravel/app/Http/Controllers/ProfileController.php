<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller
{
    function index(){
        $data = Profile::all();
        return view('profile.index', ['data' => $data]);
    }

    public function add(){
        return view('profile.add');
    }

    public function simpan(Request $req){
        $data = new Profile();
        $data -> nama_lengkap = $req->nama_lengkap;
        $data -> email = $req -> email;
        $data -> alamat = $req -> alamat;

        if($data -> save()){
            return redirect('/profile');
        }else {
            return redirect('/profile/add');
        }
    }

    public function edit($id){
        $data = Profile::where('id', $id)->get();
        return view('profile.edit', ['data'=>$data]);
    }

    public function editdata(Request $req){
        $data = Profile::find($req->id);
        $data -> nama_lengkap = $req->nama_lengkap;
        $data -> email = $req -> email;
        $data -> alamat = $req -> alamat;

        if($data -> save()){
            return redirect('/profile');
        }else {
            return redirect('/profile/edit');
        }
    }

    public function datadelete($id){
        $data = Profile::where('id',$id)->get();
        return view('profile.delete', ['data' =>$data]);
    }

    public function deleted(Request $req){
        $data = Profile::find($req->id);
        if($data -> delete()){
            return redirect('/profile');
        }else {
            return redirect('/profile/delete');
        }
    }
}
