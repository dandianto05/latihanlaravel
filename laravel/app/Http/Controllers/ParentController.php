<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parents;

class ParentController extends Controller
{
    function index(){
        $data_parents = Parents::all();
        return view('parent.index', ['data_parents' => $data_parents]);
    }
}
