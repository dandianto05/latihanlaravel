<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Child;

class ChildApiController extends Controller
{
    public function getAllChildDataRelasiParent(){
        $data = Child::with('parents_relation')->get();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $req){
        $data = new Child();
        $data->parent_id = $req->parent_id;
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->anak_ke = $req->anak_ke;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $req){
        $data = Child::find($req->id);
        $data->parent_id = $req->parent_id;
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->anak_ke = $req->anak_ke;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function getData($id){
        $data = Child::where('id', $id)->get();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function deleteData($id){
        $data = Child::find($id);
        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }



}
