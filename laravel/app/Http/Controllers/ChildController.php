<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Child;

class ChildController extends Controller
{
    function index(){
        $data = Child::with('parents_relation')->get();
        return view('child.index', ['data_child' => $data]);
    }
}
