<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index(){
        $data = array(1,3,5,7,9);
        $data2 = array("CC"=>"2200","Valve"=>"12","Whell"=>"5");
        return view('home.index', ['data'=>$data, 'data2'=>$data2]);
    }
}
