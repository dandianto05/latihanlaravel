<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parents;

class ParentApiController extends Controller
{
    public function getAllDataParent(){
        $data = Parents::all();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $req){
        $data = new Parents();
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->alamat = $req->alamat;
        $data->status = $req->status;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $req){
        $data = Parents::find($req->id);
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->alamat = $req->alamat;
        $data->status = $req->status;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function getData($id){
        $data = Parents::where('id', $id)->get();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function deleteData($id){
        $data = Parents::find($id);
        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

}
