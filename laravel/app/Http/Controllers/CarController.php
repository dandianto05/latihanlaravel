<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Car;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{
    public function index(){
        $data = Car::with('profile_relation')->get();
        // print($data[0]['model']);
        // print($data[0]['brand']);
        // print($data[0]['profile_relation']['nama_lengkap']);
        // echo("<pre>");
        // print_r($data);

        return view('car.index', ['data' =>$data]);
        
    }

    public function getCars(){
        $data = Car::with('profile_relation')->get();
        return response($data);
    }

    public function getCarsNative(){
        $data = DB::select("SELECT * FROM cars LEFT Join profiles on cars.profile_id = profiles.id");
        return response($data);
        
    }

    public function add(){
        $profile = Profile::all();
        return view('car.add', ['profile'=>$profile]);
    }
}
