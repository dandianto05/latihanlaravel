<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//API
//Table Produk
Route::get('/products_data', 'ProductsApiController@getAllData');
Route::post('/products_data/add', 'ProductsApiController@saveData');
Route::put('/products_data/edit', 'ProductsApiController@updateData');
Route::delete('/products_data/delete/{product_id}', 'ProductsApiController@deleteData');

//Table Customer
Route::get('/customer_data', 'CustomersApiController@getAllData');
Route::post('/customer_data/add', 'CustomersApiController@saveData');
Route::put('/customer_data/edit', 'CustomersApiController@updateData');
Route::delete('/customer_data/delete/{customer_id}', 'CustomersApiController@deleteData');

//Table OrderItem
Route::get('/orderItem_data', 'OrderItemApiController@getAllData');
Route::post('/orderItem_data/add', 'OrderItemApiController@saveData');
Route::put('/orderItem_data/edit', 'OrderItemApiController@updateData');
Route::delete('/orderItem_data/delete/{order_id}/{item_id}', 'OrderItemApiController@deleteData');

//Table Orders
Route::get('/orders_data', 'OrdersApiController@getAllData');
Route::post('/orders_data/add', 'OrdersApiController@saveData');
Route::put('/orders_data/edit', 'OrdersApiController@updateData');
Route::delete('/orders_data/delete/{order_id}', 'OrdersApiController@deleteData');

//Table Produck Categori
Route::get('/productsCategories_data', 'ProductsCategoriesApiController@getAllData');
Route::post('/productsCategories_data/add', 'ProductsCategoriesApiController@saveData');
Route::put('/productsCategories_data/edit', 'ProductsCategoriesApiController@updateData');
Route::delete('/productsCategories_data/delete/{category_id}', 'ProductsCategoriesApiController@deleteData');









//Table Produk View
Route::get('/product_data', 'ProductsApiController@getAllProduct')->name('products.data');
Route::get('/product_data/add', 'ProductsApiController@add')->name('products.add');
Route::post('/product_data', 'ProductsApiController@addProcess')->name('products.addProcess');
Route::get('/product_data/{id}', 'ProductsApiController@getData')->name('products.edit');
Route::put('/product_data/edit', 'ProductsApiController@updateData')->name('products.editProcess');
Route::delete('/product_data/delete/{id}', 'ProductsApiController@deleteData')->name('products.delete');