<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ProductCategories;

class ProductsCategoriesApiController extends Controller
{
    public function getAllData(){
        $data = ProductCategories::with('product_category_relation')->get();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $request){
        $data = new ProductCategories();
        $data->category_name = $request->category_name;

        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $request){
        $data = ProductCategories::find($request->category_id);
        $data->category_name = $request->category_name;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function deleteData($category_id){
        $data = ProductCategories::find($category_id);
        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


}
