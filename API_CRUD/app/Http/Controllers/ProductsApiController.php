<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Products;


class ProductsApiController extends Controller
{

    //API
    public function getAllData(){
        $data = Products::all();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $request){
        $data = new Products();
        $data->product_name = $request->product_name;
        $data->description = $request->description;
        $data->standard_cost = $request->standard_cost;
        $data->list_price = $request->list_price;
        $data->category_id = $request->category_id;

        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $request){
        $data = Products::find($request->product_id);
        $data->product_name = $request->product_name;
        $data->description = $request->description;
        $data->standard_cost = $request->standard_cost;
        $data->list_price = $request->list_price;
        $data->category_id = $request->category_id;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function deleteData($product_id){
        $data = Products::find($product_id);
        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }






    //View
    public function getAllProduct(){
        $product = DB::select('SELECT * FROM batch258.products');
        return view ('products.data', ['product' => $product]);
    }

    public function add()
    {
        return view ('products.add');
    }
    
    public function addProcess(Request $request)
    {
        DB::table('batch258.products')->insert([
            'product_id' => $request->product_id,
            'product_name' => $request->product_name,
            'description' => $request->description,
            'standard_cost' => $request->standard_cost,
            'list_price' => $request->list_price,
            'category_id' => $request->category_id,
        ]);

        return redirect()->route('products.add')->with('status', 'Data berhasil ditambah!');
    }

}
