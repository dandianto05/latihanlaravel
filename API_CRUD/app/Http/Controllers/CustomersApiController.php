<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Customers;

class CustomersApiController extends Controller
{
    public function getAllData(){
        $data = Customers::all();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $request){
        $data = new Customers();
        $data->name = $request->name;
        $data->address = $request->address;
        $data->website = $request->website;
        $data->credit_limit = $request->credit_limit;

        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $request){
        $data = Customers::find($request->customer_id);
        $data->name = $request->name;
        $data->address = $request->address;
        $data->website = $request->website;
        $data->credit_limit = $request->credit_limit;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function deleteData($customer_id){
        $data = Customers::find($customer_id);
        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }
}
