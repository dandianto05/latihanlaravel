<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\OrderItem;

class OrderItemApiController extends Controller
{
    public function getAllData(){
        $data = OrderItem::all();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $request){
        $data = new OrderItem();
        $data->order_id = $request->order_id;
        $data->item_id = $request->item_id;
        $data->product_id = $request->product_id;
        $data->quantity = $request->quantity;
        $data->unit_price = $request->unit_price;

        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $request){
      
        $data = OrderItem::find($request->order_id)->find($request->item_id)->first();

        $data->product_id = $request->product_id;
        $data->quantity = $request->quantity;
        $data->unit_price = $request->unit_price;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function deleteData($order_id, $item_id){
        // $data = OrderItem::where('order_id', $order_id)
        // ->where('item_id', $item_id);
        $data = OrderItem::find($order_id)->find($item_id)->first();

        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }
}
