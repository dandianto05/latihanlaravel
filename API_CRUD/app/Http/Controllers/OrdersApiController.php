<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Orders;

class OrdersApiController extends Controller
{
    public function getAllData(){
        $data = Orders::all();
        if(count($data)>0){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $request){
        $data = new Orders();
        $data->customer_id = $request->customer_id;
        $data->status = $request->status;
        $data->salesman_id = $request->salesman_id;
        $data->order_date = $request->order_date;

        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $request){
        $data = Orders::find($request->order_id);
        $data->customer_id = $request->customer_id;
        $data->status = $request->status;
        $data->salesman_id = $request->salesman_id;
        $data->order_date = $request->order_date;
        if($data->save()){
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function deleteData($order_id){
        $data = Orders::find($order_id);
        if($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        }else{
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }
}
