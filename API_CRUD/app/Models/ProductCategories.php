<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'batch258.product_categories';
    protected $primaryKey = 'category_id'; 

    public function product_category_relation()
    {
        return $this->belongsTo('App\Models\Products','category_id');
    }

}
