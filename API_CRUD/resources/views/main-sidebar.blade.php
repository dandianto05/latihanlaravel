<div class="main-sidebar sidebar-style-2" tabindex="1" style="overflow: hidden; outline: none;">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">

        <a href="/">TABLE API</a>
       
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">API</a>
      </div>
      <ul class="sidebar-menu">
        <li class="menu-header">Data</li>
        <li class="dropdown">
          <a href="{{ route('products.data') }}" class="nav-link"><i class="fas fa-fire"></i><span>Products</span></a>
        </li>
      </ul>

      <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="/" class="btn btn-primary btn-lg btn-block btn-icon-split">
          <i class="fas fa-rocket"></i> Home Page
        </a>
      </div>
      
    </aside>
  </div>
  {{-- active --}}