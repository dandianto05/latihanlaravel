@extends('main')

@section('title','Add Data Product ')

@section('main-content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Products</h1>
            </div>

        <div class="section-body">
 
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header justify-content-between">

                    <div class="pull-left">
                        <h4>Add Data Product</h4>
                    </div>

                    <div class="pull-right">
                        <a href="{{ route('products.data') }}" class="btn btn-icon btn-secondary">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>

                </div>
                
                <div class="card-body">
                   
                    
                        <div class="row">
                            <div class="col-md-4 offset-md-4">
                                <form action="{{ route('products.addProcess')}}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label>Product ID</label>
                                        <input type="text" name="product_id" class="form-control" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input type="text" name="product_name" class="form-control" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input type="text" name="description" class="form-control" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <label>Standard Cost</label>
                                        <input type="text" name="standard_cost" class="form-control" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <label>List Price</label>
                                        <input type="text" name="list_price" class="form-control" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <input type="text" name="category_id" class="form-control" autofocus required>
                                    </div>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </form>
                            </div>
                        </div>
                 
                </div>
            </div>
        </div>
    </div>
    
@endsection