@extends('main')

@section('title', 'Data Product ')

@section('css')
    <link rel="stylesheet" href="{{ asset('style/assets/vendor/datatable/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/assets/vendor/datatable/dataTables.bootstrap4.min.css') }}">
@endsection

@section('main-content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Product</h1>
            </div>

        <div class="section-body">
    
@endsection

@section('content')

    <div class="row">
        <div class="col-12">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header justify-content-between">

                    <div class="pull-left">
                        <h4>Data Product</h4>
                    </div>

                    <div class="pull-right">
                        <a href="{{ route('products.add' )}}" class="btn btn-icon btn-success">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </div>

                </div>
                
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-md" id="datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Product ID</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>Standard Cost</th>
                                <th>List Price</th>
                                <th>Category ID</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product as $item)
                                <tr>
                                    <td>{{ $loop ->iteration }}</td>
                                    <td>{{ $item->product_id }}</td>
                                    <td>{{ $item->product_name}}</td>
                                    <td>{{ $item->description }}</td>
                                    <td>{{ $item->standard_cost }}</td>
                                    <td>{{ $item->list_price }}</td>
                                    <td>{{ $item->category_id }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('products.edit',$item->product_id )}}" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a>
                                        <form action="{{ route('products.delete',$item->product_id )}}" method="POST" class="d-inline" onsubmit="return confirm('Yakin hapus data?')">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-icon btn-danger"><i class="far fa-trash-alt"></i></button>    
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js-mid')
    <script src="{{ asset('style/assets/vendor/datatable/datatables.min.js') }}"></script>    
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        } );
    </script>
@endsection